def sigma2(n):
	factors = [i*i for i in range(1,int(n/2)+1) if n%i==0]
	factors.append(n**2)
	factorsSum = sum(factors)
	return factorsSum


def SIGMA2(n):
	toFindSum = [sigma2(num) for num in range(1,n+1)]
	finalSum = sum(toFindSum)
	return finalSum

num = SIGMA2(1015) 
print(num)
finalResult = num % 109
print(finalResult)